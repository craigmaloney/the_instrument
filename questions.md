# The Game

## Intro (Invocation)

What kind of instrument are you?

Who or what created you, and where?

When were you created, and if you know, how old are you?

Approximately how long did it take to make you?

What materials were used to create you?

Are there others like you or are you unique?

## First Movement (Allegro)

Who was the first performer to play you?

What was that performer's relationship to your creator?

How skilled were they in playing you?

What music did this performer play with you?

What was the most memorable performance with this performer?

Was there an audience and if so how did they react?

## Second Movement (Scherzo)

Who was the next interesting performer / owner to play you?

Did this performer know the first performer, and if so what was their relationship?

What music did this performer play with you?

What was different about this performer / owner compared with the first performer / owner?

What was their most memorable performance with you?

Was there an audience and if so how did they react?

## Interlude

What notes or sounds can you play well?

What notes or sounds are horrible on you?

Would an expert be able to recognize these notes or sounds as unique to you or others like you?

Is there a style of playing that can mitigate or enhance these notes or sounds?

## Third movement (Andante)

Who is your current owner?

Do they also perform with you, and if not who is that performer?

If you had to guess, how many performers / owners have you had thus far?

Does your current performer have any relationship with your previous performers, and if so what are those relationships?

What kinds of music do you play now?

What is the most striking difference between your current owner / performer and your first owner / performer?

Have there been any more memorable performances, and if so what were they?

## Con amore

Were you ever lost or stolen? If so, were you recovered?

Were you ever broken, and if so, how were you broken and were you ever repaired?

If you were repaired were you made whole again or are there little quirks in how you are played?

## Fourth movement (Lento)

Which performer / owner would you bring back for one more encore?

Which performer / owner did you despise?

Who do you wish would be your next performer / owner?

Which piece do you wish would be played on you?

Which piece would you like to perform again?

## Coda

Where do you currently reside?

Where will you go from here?

Will you ever be played again?

## Fin
