all: the_instrument.pdf

the_instrument.pdf : intro.md example.md instructions.md questions.md references.md
	pandoc --toc --include-in-header titlesec.tex -o the_instrument.pdf intro.md example.md instructions.md questions.md references.md instruments.md

clean:
	rm *.pdf
