# Example instruments

Here is a list of example instruments. You may select any one of these through whatever means you wish. There are 100 of them so you can use a 1d100 to select one at random, or you can just read through them and find one that sparks your interest to become that instrument and learn more about it.

\Begin{multicols}{2}

1. Piano
1. Harpsichord
1. Clav
1. Celesta
1. Glockenspiel
1. Music Box
1. Vibraphone
1. Marimba
1. Xylophone
1. Tubular Bells
1. Dulcimer
1. Organ
1. Accordion
1. Harmonica
1. Guitar
1. Bass
1. Harmonica
1. Guitar
1. Bass
1. Violin
1. Viola
1. Cello
1. Contrabass
1. Timpani
1. Trumpet
1. Trombone
1. Tuba
1. French Horn
1. Saxophone
1. Oboe
1. Bassoon
1. Clarinet
1. Piccolo
1. Flute
1. Recorder
1. Pan Flute
1. A tuned bottle
1. Shakuhachi
1. Whistle
1. Ocarina
1. Synthesizer
1. Sitar
1. Banjo
1. Shamisen
1. Koto
1. Kalimba
1. Bagpipe
1. Fiddle
1. Shanai 
1. A bell
1. Agogo
1. Steel Drums
1. Woodblock
1. Taiko Drum
1. Drumset
1. Telephone
1. Helicopter
1. Turntable / Record Player
1. An old game console
1. Marching drum
1. Erhu
1. Sarangi
1. Cimbalom
1. Theremin
1. Glass Harmonica
1. Handpan
1. Chapman Stick
1. Pots and pans
1. HUrdy-Gurdy
1. Lute
1. Gong / Tam Tam
1. Kazoo
1. Harp / Lyre
1. Tabla
1. Sarrusophone
1. Mandolin
1. Player Piano
1. Computer
1. Erhu
1. Tambourine
1. Dholak
1. Congas
1. Drum machine
1. Melodica
1. Yu
1. Virginal
1. Sackbut
1. Serpent
1. Tabor
1. Tape Recorder
1. Mixing board
1. Maraccas
1. Castanets
1. Ratchet
1. Güiro
1. Cajon
1. Djembe
1. Shaker
1. Mellotron
1. Typewriter 

\End{multicols}
