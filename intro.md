# The Instrument

By Craig Maloney

Version 0.3 pre-release alpha playtest draft.

\copyright Copyright 2022 Craig Maloney. Will be released under a [Creative Commons Attribution-ShareAlike 4.0 International (CC-BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0) license. At the moment this is still in draft mode, so please refrain from sharing this until the final release. Thank you!

## Introduction

Ever since there have been musicians there are the instruments they have played. Instruments become an extension of their performers, taking on the characteristics of a trusted companion, friend, or partner. Instruments are an integral part of any musical performance. A good instrument can enhance the performance, and generate a feedback loop between instrument and performer that creates legendary performances. Some instruments are also passed along from performer to performer, tracing a lineage over hundreds of years and dozens of different performers. Each performer can leave their mark (figuratively or literally) with the instrument. Each performance adds to the legacy of these instruments. 

The Instrument is a lonely game that tells the stories of these instruments. It is played with one player and a notebook (or any other writing device). Games can be as long or as short as you wish them to be. 

## Content Warning: 

This game has questions about instruments that have been inadvertently abandoned, lost, or broken. It's also about the relationships between instruments and the folks that play them. If these questions stir feelings in you that you'd rather not explore then please review the questions first before playing and strike out any that may cause you to explore feelings you'd rather not explore at this time. Alternatively, if you are asked to answer a question that stirs up feelings you'd rather not explore at this time then please feel free to skip the question. We'll have more on this in the "How to Play" section.

## Inspiration:

* [Of the Woods](https://www.drivethrurpg.com/product/209823/Of-the-Woods-Lonely-Games-of-Imagination) by Beau Jágr Sheldon, Kimberley Lam, Moyra Turkington, Meera Barry, Chris Bennett, and Adam McConnaughey. Beau deserves credit for coning the term "lonely game", which is a perfect term for this game. Beau also deserves credit for the concept of consent in this game, which is something we should allow every player in our games, including the ones we play by ourselves. 
* [The Artefact](https://mouseholepress.itch.io/artefact) by Jack Harrison, which inspired me to think about objects in a different light. (This game is based on the Lost & Found SRD ([https://lostandfound.games](https://lostandfound.games)))
* Various stories of instruments, including the examples found in the "Examples of Famous Stringed Instruments" section. The relationship between performer and instrument has fascinated me, and I wanted to highlight how special this relationship can be.
