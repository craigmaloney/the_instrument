# References and further reading

* Haylock, Julian. "Playing with Paganini." *BBC Music Magazine* Apr. 2021: 38-40. Print.
* Wikipedia contributors. "Niccolò Paganini." Wikipedia, The Free Encyclopedia. Wikipedia, The Free Encyclopedia, 5 Jun. 2021. Web. 6 Jun. 2021.
* Wikipedia contributors. "Il Cannone Guarnerius." Wikipedia, The Free Encyclopedia. Wikipedia, The Free Encyclopedia, 10 Mar. 2021. Web. 6 Jun. 2021.
* Wikipedia contributors. "Yo-Yo Ma." Wikipedia, The Free Encyclopedia. Wikipedia, The Free Encyclopedia, 6 Dec. 2021. Web. 7 Dec. 2021.
* The MIDI General Instrument specification for inspiration for instruments
